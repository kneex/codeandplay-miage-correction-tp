package fr.miage;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.Properties;

public class BattleMain {

    public static void main(String[] args) throws ParseException, IOException {
        Options options = new Options();
        options.addOption("p", false, "Ping le serveur de jeu");
        options.addOption("config", false, "Affiche la configuration du programme");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        if (cmd.hasOption("p")) {
            System.out.println("pong");
        }

        if (cmd.hasOption("config")) {
            Properties properties = new Properties();
            properties.load(BattleMain.class.getClassLoader().getResourceAsStream("application.properties"));
            System.out.println("kart.server.base-url : " + properties.getProperty("kart.server.base-url"));
            System.out.println("kart.client.team.name : " + properties.getProperty("kart.client.team.name"));
            System.out.println("kart.client.team.password : " + properties.getProperty("kart.client.team.password"));
        }
    }

}
